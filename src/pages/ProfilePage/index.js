import React from "react";
import * as Section from "components/PageSection";
import { ProfileContent } from './ProfileContent/ProfileContent';

export const Profile = () => (

    <Section.Wrapper>
        <Section.Content>
            <ProfileContent />
        </Section.Content>
    </Section.Wrapper>
    
)