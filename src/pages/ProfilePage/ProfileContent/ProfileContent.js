import React, { useState, useEffect } from "react";

export function ProfileContent() {

    useEffect(() => {
        fetchUser()
    }, []);

    const [users, setUser] = useState([]);

    const fetchUser = async () => {
        const data = await fetch('https://api.github.com/users/JanBanas');
        const users = await data.json();
        console.log(users.name);
        setUser(users);
    }
    

    return (
        <div>
            <h2>{users.name}</h2>
            <img src={users.avatar_url} alt={users.name} style={{ borderRadius: '10px' }} />
        </div>
    )

}