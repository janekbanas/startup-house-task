import React from 'react';

export const SingleSlide = ({image, name}) => (
    <img src={image} alt={name} className="slide" />
);