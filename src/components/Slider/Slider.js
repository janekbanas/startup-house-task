import React from 'react';
import { SingleSlide } from './SingleSlide/SingleSlide';
import { ImgBase } from './ImgBase';
import './Slider.css';

export const Slider = () => (
    <div className="sliderWrapper">
        <div className="slider">
            <div className="slide-track">
                {ImgBase.map(item => (
                    <SingleSlide key={item.name} {...item} />
                ))}
            </div>
        </div>
    </div>
);