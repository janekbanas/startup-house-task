import dog1 from '../../assets/photos/slide-1.jpg';
import dog2 from '../../assets/photos/slide-2.jpg';
import dog3 from '../../assets/photos/slide-3.jpg';
import dog4 from '../../assets/photos/slide-4.jpg';
import dog5 from '../../assets/photos/slide-5.jpg';
import dog6 from '../../assets/photos/slide-6.jpg';

export const ImgBase = [{
        image: dog1,
        name: 'DogFromSlideOne',
    },
    {
        image: dog2,
        name: 'DogFromSlideTwo',
    },
    {
        image: dog3,
        name: 'DogFromSlideThree',
    },
    {
        image: dog4,
        name: 'DogFromSlideFour',
    },
    {
        image: dog5,
        name: 'DogFromSlideFive',
    },
    {
        image: dog6,
        name: 'DogFromSlideSix',
    },
]