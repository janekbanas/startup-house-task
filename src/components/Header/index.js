import React from "react";

import { Root, Wrapper, Logo } from "./styles";
import { Menu } from "./Menu";

export const Header = () => (
  <Root>
    <Wrapper>
      <Logo />
      <Menu />
    </Wrapper>
  </Root>
);
