import React from 'react';
import './Menu.css'
import { Link } from 'react-router-dom';

export const Menu = () => (
    <ul className="menu__wrapper">
        <Link to="/" className="menu__item">
            <li>Start</li>
        </Link>
        <Link to="myprofile" className="menu__item">
            <li>My Profile</li>
        </Link>
    </ul>
)