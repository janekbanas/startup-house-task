import React from "react";
import { ThemeProvider } from "styled-components";
import { Normalize } from "styled-normalize";

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { Header } from "components/Header";
import { Home } from "pages/Home";
import { Profile } from "pages/ProfilePage";

import { GlobalStyle, theme } from "./styles";

export const App = () => (
  <Router>
    <ThemeProvider theme={theme}>
      <Normalize />
      <GlobalStyle />
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/myprofile" component={Profile} />
      </Switch>
    </ThemeProvider>
  </Router>
);
